#!/bin/bash

iptables-save | grep -Ev "#|:" | sort > /tmp/fwmemrules
grep -Ev "#|:" /etc/iptables/rules.v4 | sort > /tmp/fwfilerules

if diff /tmp/active /tmp/file &> /dev/null ; then
	echo "false"
else
	echo "true"
fi

rm -f /tmp/fwmemrules /tmp/fwfilerules || true 
