Facter.add('is_firewall_change') do
  setcode do
    Facter::Core::Execution.execute('/usr/local/bin/fwchange.sh')
  end
end
