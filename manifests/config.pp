# @summary
#   This class handles the configuration file's.
#
# @api private
#
class firewall::config inherits firewall {

  $firewall::config_files.each |String $file| {
    file { "${firewall::configuration_directory}/${file}":
      ensure  => file,
      content => epp("firewall/${::role}${firewall::configuration_directory}/${file}.epp"),
    }
  }

  file { "${firewall::script_path}${firewall::script_name}":
    ensure  => file,
    mode    => '0755',
    content => file("firewall/${firewall::script_name}"),
  }

  # reload netfilter-persistent if actives rules are differents like rules.v4
  if $facts['is_firewall_change'] {
    exec {"${firewall::service_name}-restart":
      command => "systemctl restart ${firewall::service_name}",
      path    => ['/bin/'],
    }
  }

}
