# firewall
#
# @summary Init class for firewall module, includes other classes
#
# @example Declare this class in site.pp
#   class firewall {
#   configuration_directory => '/etc/iptables/',
#   package_name            => netfilter-persistent',
#   service_name            => 'netfilter-persistent',
#   }
#
# @param service_name
#   The real service name's. Default value: undef.
#
# @param service_enable
#   true or false. Default value: false.
#
# @param mail_name
#   firewall public signature. Default value: undef.
#
# @param service_ensure
#   "running" ou "stopped"
#
# @param service_manage
#   true or false. Default value: true.
#
# @param firewall_packages
#   List of firewall packages. Array. Default value: see data/os/*.
#
# @param script_path
#   true or false. Default value: None.
#
class firewall (

  Array   $config_files,
  String  $configuration_directory,

  Boolean $service_enable                       = true,
  String  $service_ensure                       = 'running',
  Boolean $service_manage                       = true,
  Optional[String] $service_name,
  Optional[String] $script_path,
  Optional[String] $script_name,
  Optional[Array]  $firewall_packages,

  ) {

  anchor { 'firewall::begin': } -> class { '::firewall::install': } -> class { '::firewall::config': } ~> class { '::firewall::service': } -> anchor { 'firewall::end': }

}
