# @summary
#   This class handles firewall packages.
#
# @api private
#
class firewall::install inherits firewall {

  $firewall::firewall_packages.each |String $package|{
    package { $package:
      ensure => installed,
    }
  }

}
