# @summary
#   This class handles the firewall service.
#
# @api private
#
class firewall::service inherits firewall {

  if ! ($firewall::service_ensure in [ 'running', 'stopped' ]) {
    fail('service_ensure parameter must be running or stopped')
  }
  if $firewall::service_manage == true {
    service { $firewall::service_name:
      ensure     => $firewall::service_ensure,
      enable     => $firewall::service_enable,
      provider   => systemd,
      hasstatus  => true,
      hasrestart => true,
    }
  }

}
