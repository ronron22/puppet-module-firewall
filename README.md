
# architux_firewall

This module manage netfilter rules by using the netfilter-persistent program.

I decided to define firewall rules not by server name but by server roles.  

Important point, in case of actives rules will be differents from the rules.v4 file's, this last will be load. for that, i use one custom fact and one script.

## Utilisation

### roles tree example 

with 2 roles in my case.

```bash
templates/
|-- mutu
|   `-- etc
|       `-- iptables
|           `-- rules.v4.epp
`-- mutu_slave
    `-- etc
        `-- iptables
            `-- rules.v4.epp
```

## Limitations

You can't manage firewall rules by puppet modules or system services.

### netfilter-persistent

netfilter-persistent only exist on Debian system, that why this module can manage only this systems.

### roles

You must define role in hiera for using this module

## Facts

This module push an shell script in /usr/local/bin/ and an external fact.

The facts is used for detective an delta between file rules and memory rules.

## Testing firewall rule's

```bash
~#  iptables-restore  < /etc/iptables/rules.v4 
```
